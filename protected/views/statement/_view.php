<?php
/* @var $this StatementController */
/* @var $data Statement */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('countLT')); ?>:</b>
	<?php echo CHtml::encode($data->countLT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('countHT')); ?>:</b>
	<?php echo CHtml::encode($data->countHT); ?>
	<br />
        
<!--
	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />
-->

</div>