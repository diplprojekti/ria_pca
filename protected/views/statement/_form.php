<?php
/* @var $this StatementController */
/* @var $model Statement */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'statement-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Polja s <span class="required">*</span> su potrebna.</p>

	<?php echo $form->errorSummary($model); ?>
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>
-->

	<div class="row">
		<?php echo $form->labelEx($model,'countLT'); ?>
		<?php echo $form->textField($model,'countLT'); ?>
		<?php echo $form->error($model,'countLT'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'countHT'); ?>
		<?php echo $form->textField($model,'countHT'); ?>
		<?php echo $form->error($model,'countHT'); ?>
	</div>
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>16,'maxlength'=>16)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
-->
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Izradi' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->