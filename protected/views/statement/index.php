<?php
/* @var $this StatementController */
/* @var $dataProvider CActiveDataProvider */

$this->menu=array(
	array('label'=>'Napravi obračun', 'url'=>array('create')),
	array('label'=>'Upravljaj obračunima', 'url'=>array('admin')),
);
?>

<h1>Obračuni</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
