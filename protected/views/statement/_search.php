<?php
/* @var $this StatementController */
/* @var $model Statement */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'countLT'); ?>
		<?php echo $form->textField($model,'countLT'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'countHT'); ?>
		<?php echo $form->textField($model,'countHT'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>16,'maxlength'=>16)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Traži'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->