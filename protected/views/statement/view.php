<?php
/* @var $this StatementController */
/* @var $model Statement */

$this->menu=array(
	array('label'=>'Prikaži obračune', 'url'=>array('index')),
	array('label'=>'Napravi obračun', 'url'=>array('create')),
	array('label'=>'Izbriši obračun', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Upravljaj obračunima', 'url'=>array('admin')),
);
?>

<h1>Obračun #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'date',
		'countLT',
		'countHT',
		'username',
	),
)); ?>
