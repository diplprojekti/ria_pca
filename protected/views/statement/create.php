<?php
/* @var $this StatementController */
/* @var $model Statement */

$this->menu=array(
	array('label'=>'Prikaži obračune', 'url'=>array('index')),
	array('label'=>'Upravljaj obračunima', 'url'=>array('admin')),
);
?>

<h1>Napravi obračun</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>