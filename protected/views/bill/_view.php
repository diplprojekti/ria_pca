<?php
/* @var $this BillController */
/* @var $data Bill */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_from')); ?>:</b>
	<?php echo CHtml::encode($data->date_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_to')); ?>:</b>
	<?php echo CHtml::encode($data->date_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priceLT')); ?>:</b>
	<?php echo CHtml::encode($data->priceLT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('priceHT')); ?>:</b>
	<?php echo CHtml::encode($data->priceHT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_statement1')); ?>:</b>
	<?php echo CHtml::encode($data->id_statement1); ?>
	<br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('id_statement2')); ?>:</b>
	<?php echo CHtml::encode($data->id_statement2); ?>
	<br />

</div>