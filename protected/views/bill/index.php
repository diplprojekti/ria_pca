<?php
/* @var $this BillController */
/* @var $dataProvider CActiveDataProvider */

$this->menu=array(
	array('label'=>'Upravljaj računima', 'url'=>array('admin')),
);
?>

<h1>Računi</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
