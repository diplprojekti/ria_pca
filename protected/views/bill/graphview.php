<?php
    Yii::app()->clientScript->registerCoreScript('jquery'); 
    Yii::app()->clientScript->registerCoreScript('jquery.ui');
/* @var $this BillController */
/* @var $model Bill */

?>

<h1>Pregled računa</h1>

<?php $this->widget('jqBarGraph',array('values'=>$arrayall,
            'type'=>'multi',
            'width'=>300,
            'colors'=>array('#00CC00','#3333FF'),
            'legend'=>true,
            'legends'=>array('PriceLT','PriceHT'),
            'title'=>false
            )
        );
?>

