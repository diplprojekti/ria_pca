<?php
/* @var $this BillController */
/* @var $model Bill */


$this->menu=array(
	array('label'=>'Prikaži račune', 'url'=>array('index')),
	array('label'=>'Izbriši račun', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Upravljaj računima', 'url'=>array('admin')),
);
?>

<h1>Račun #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'date_from',
		'date_to',
		'priceLT',
		'priceHT',
		'username',
		'id_statement1',
		'id_statement2',
	),
)); ?>
