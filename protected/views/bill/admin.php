<?php
/* @var $this BillController */
/* @var $model Bill */

$this->menu=array(
	array('label'=>'Prikaži račune', 'url'=>array('index')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#bill-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Upravljaj računima</h1>

<?php echo CHtml::link('Napredno pretraživanje','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bill-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'date_from',
		'date_to',
		'priceLT',
		'priceHT',
		'username',
		'id_statement1',
		'id_statement2',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
