<?php
/* @var $this UserController */
/* @var $model User */

$this->menu=array(
	array('label'=>'Prikaži korisnike', 'url'=>array('index')),
	array('label'=>'Kreiraj korisnika', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Upravljaj korisnicima</h1>



<?php echo CHtml::link('Napredno pretraživanje','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'username',
		'password',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
