<?php
/* @var $this UserController */
/* @var $model User */


$this->menu=array(
	array('label'=>'Prijava', 'url'=>array('login')),
);
?>

<h1>Registracija</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>