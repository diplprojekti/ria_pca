<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->menu=array(
	array('label'=>'Kreiraj korisnika', 'url'=>array('create')),
	array('label'=>'Uredi korisnika', 'url'=>array('admin')),
);
?>

<h1>Korisnici</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
