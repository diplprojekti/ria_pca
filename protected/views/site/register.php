<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Registracija';

$this->menu=array(
	array('label'=>'Prijava', 'url'=>array('login')),
    );
?>

<h1>Registracija</h1>

<p>Ispunite formular svojim podacima:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
    )); 
?>

	<p class="note">Polja s <span class="required">*</span> su potrebna.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
		
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Registracija'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->