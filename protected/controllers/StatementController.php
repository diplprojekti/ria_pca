<?php

class StatementController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                $username = Yii::app()->user->getId();
		$model=new Statement;
                $model->username = $username;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Statement']))
		{
                    //Get most recent statement with user ID
                    $statement = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from('statement')
                            ->where('id=(select MAX(id) from statement where username=:username)', array(':username'=>$username))
                            ->queryAll();
                    
                    $model->attributes=$_POST['Statement'];

                    
                        
                    if($model->save()){
                        if ($statement!=null){
                            $myvalue = $statement[0];
                            $billmodel = new Bill;
                            $billmodel->username = $username;
                            $billmodel->date_from = $myvalue['date'];
                            $billmodel->id_statement1 = $myvalue['id'];
                            $billmodel->id_statement2 = $model->id;
                            $billmodel->priceLT = $this->calcLT($myvalue['countLT'],$model->countLT);
                            $billmodel->priceHT = $this->calcHT($myvalue['countHT'], $model->countHT);
                            $billmodel->save(false);
                        }
                        $this->redirect(array('view','id'=>$model->id));
                    }
                    
                }
                $this->render('create',array(
                            'model'=>$model,
                ));
        }
        
        /*
         * Price calculation functions
         */
        public function calcLT($oldCount,$newCount){
            $url = 'http://mehonjic.riteh.hexis.hr/api/index.php';
            $jsonvalues = Yii::app()->curl->get($url);
            $jsonvalues = json_decode($jsonvalues);
            $priceLt = (double)$jsonvalues[1];
            //Calculate the price
            return $priceLt * (double)($newCount - $oldCount);
        }
        
        public function calcHT($oldCount,$newCount){
            $url = 'http://mehonjic.riteh.hexis.hr/api/index.php';
            $jsonvalues = Yii::app()->curl->get($url);
            $jsonvalues = json_decode($jsonvalues);
            $priceHt = (double)$jsonvalues[0];
            //Calculate the price
            return $priceHt * (double)($newCount - $oldCount);
            
        }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
                $url = Yii::app()->baseUrl.'/index.php/statement/';
                $this->redirect($url); //Redirect to list all statements

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
                $username = Yii::app()->user->getId();
		$model=new Statement;
                $model->username = $username;
                $dataProvider=new CActiveDataProvider('Statement', array(
                    'criteria'=>array(
                    'condition'=>'username="'.$username.'"',
                    ),
                ));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Statement('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Statement']))
			$model->attributes=$_GET['Statement'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Statement the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Statement::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Statement $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='statement-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
}
