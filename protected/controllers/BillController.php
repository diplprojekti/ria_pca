<?php

class BillController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update','delete','graphview'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

        /**
         * Display the graphical view
         * @param type $id
         */
        public function actionGraphview()
	{
                $username = Yii::app()->user->getId();
                $bill = Yii::app()->db->createCommand()
                            ->select('priceLT,priceHT,date_from,date_to')
                            ->from('bill')
                            ->where('username=:username', array(':username'=>$username))
                            ->queryAll();
                foreach ($bill as $onebill){
                    $priceLT = $onebill['priceLT'];
                    $priceHT = $onebill['priceHT'];
                    $date = date("Y/m/d", strtotime($onebill['date_from'])).' - '.date("Y/m/d", strtotime($onebill['date_to']));
                    $temparray=array(array((double)$priceLT,(double)$priceHT),$date);
                    $arrayall[] = $temparray;
                }
		$this->render('graphview',array(
			'arrayall'=>$arrayall,
		));
                 
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
                $url = Yii::app()->baseUrl.'/index.php/bill/';
                $this->redirect($url); //Redirect to list all bills    
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$username = Yii::app()->user->getId();
		$model=new Statement;
                $model->username = $username;
                $dataProvider=new CActiveDataProvider('Bill', array(
                    'criteria'=>array(
                    'condition'=>'username="'.$username.'"',
                    ),
                ));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Bill('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Bill']))
			$model->attributes=$_GET['Bill'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Bill the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Bill::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
	/**
	 * Performs the AJAX validation.
	 * @param Bill $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bill-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
