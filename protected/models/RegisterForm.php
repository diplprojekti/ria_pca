<?php

class RegisterForm extends CFormModel
{
        public $username;
        public $password;
        private $_identity;

        /**
         * Declares the validation rules.
         * The rules state that username, password & email are required,
         * and username & email needs to be unique.
         */
        public function rules()
        {
            return array(
                array('username, password', 'required'), // username and password are required
                array('username', 'unique'), // make sure username and email are unique
                );
        }

        /**
         * Declares attribute labels.
         */
        public function attributeLabels()
        {
                return array(
                        'username'=>'Vaše korisničko ime',
                        'password'=>'Vaša lozinka'
                );
        }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

