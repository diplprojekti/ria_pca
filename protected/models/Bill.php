<?php

/**
 * This is the model class for table "bill".
 *
 * The followings are the available columns in table 'bill':
 * @property integer $id
 * @property string $date_from
 * @property string $date_to
 * @property double $priceLT
 * @property double $priceHT
 * @property string $username
 * @property integer $id_statement1
 * @property integer $id_statement2
 */
class Bill extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bill';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date_to, priceLT, priceHT, username, id_statement1, id_statement2', 'required'),
			array('id_statement1, id_statement2', 'numerical', 'integerOnly'=>true),
			array('priceLT, priceHT', 'numerical'),
			array('username', 'length', 'max'=>16),
			array('date_from', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date_from, date_to, priceLT, priceHT, username, id_statement1, id_statement2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date_from' => 'Datum od',
			'date_to' => 'Datum do',
			'priceLT' => 'Cijena niske tarife',
			'priceHT' => 'Cijena visoke tarife',
			'username' => 'Korisničko ime',
			'id_statement1' => 'Id obračuna 1',
			'id_statement2' => 'Id obračuna 2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date_from',$this->date_from,true);
		$criteria->compare('date_to',$this->date_to,true);
		$criteria->compare('priceLT',$this->priceLT);
		$criteria->compare('priceHT',$this->priceHT);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('id_statement1',$this->id_statement1);
		$criteria->compare('id_statement2',$this->id_statement2);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bill the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
